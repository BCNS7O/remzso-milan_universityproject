/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import model.Sorok;
import java.util.Random;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author khrone
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SorController implements Serializable {
    @XmlTransient
    Random ran1 = new Random();
    private static SorController instance;
    @XmlElement
    private List<Sorok> Ujsorok = new ArrayList<Sorok>();
    @XmlTransient
    String[] Nevek = {"Folyekon kenyer","Ipa 88","Vipera","Bulldog","Lufi","Narancs varazs","Napfelkelte","Mesterviz","Fuszerbomba","Lagy Folyo","Vedd meg","Fitt sor","Teszta dagaszto","Menyet","Western","Galaxis","A kapitany","Hegyteto","Game over","Trademarked"};
    @XmlTransient
    String[] Tipusok = {"Barna", "Ipa", "Apa", "Stout", "Lager"};
    @XmlTransient
    String[] Glutenek = {"Nem","Igen"};
    public SorController()
    { for (int i = 0; i < 2; i++) {
            Sorok ujsor = new Sorok();
            ujsor.setNev(Nevek[ran1.nextInt(20)]);
            ujsor.setAr(ran1.nextInt(2000));
            ujsor.setTipus(Tipusok[ran1.nextInt(5)]);
            ujsor.setAlkohol(ran1.nextInt(15));
            ujsor.setGluten(Glutenek[ran1.nextInt(2)]);
            
            Ujsorok.add(ujsor);
        }
    }

    
    
    public static SorController getInstance(){
        if (instance == null) {
            instance = new SorController();
        }
        return instance;
    }
    
    public List<Sorok> getUjsorok() {
        return Ujsorok;
    }
    
    
}
