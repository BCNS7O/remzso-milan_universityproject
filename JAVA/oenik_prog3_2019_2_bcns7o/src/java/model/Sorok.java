/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author khrone
 */
@XmlRootElement(name = "Sörök")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sorok implements Serializable {
    // Név, Ár, Tipus, Alkohol, Beosztott_id, Glutén
    @XmlElement
    private String nev;
    @XmlElement
    private int ar;
    @XmlElement
    private String tipus;
    @XmlElement
    private int alkohol;
    @XmlElement
    private String gluten;
    
    public Sorok(){
 
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public int getAr() {
        return ar;
    }

    public void setAr(int ar) {
        this.ar = ar;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public int getAlkohol() {
        return alkohol;
    }

    public void setAlkohol(int alkohol) {
        this.alkohol = alkohol;
    }

    public String isGluten() {
        return gluten;
    }

    public void setGluten(String gluten) {
        this.gluten = gluten;
    }
    
    
}
