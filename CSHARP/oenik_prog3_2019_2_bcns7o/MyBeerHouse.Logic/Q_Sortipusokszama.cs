﻿// <copyright file="Q_Sortipusokszama.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;

    /// <summary>
    /// This class exists to be the parameter to the generic Non crud interface.
    /// </summary>
    public class Q_Sortipusokszama
    {
        private string név;
        private int átlag;

        /// <summary>
        /// Initializes a new instance of the <see cref="Q_Sortipusokszama"/> class.
        /// </summary>
        public Q_Sortipusokszama()
        {
        }

        /// <summary>
        /// Gets or sets the name property of the Query.
        /// </summary>
        public string Név { get => this.név; set => this.név = value; }

        /// <summary>
        /// Gets or sets the average property of the Query.
        /// </summary>
        public int Átlag { get => this.átlag; set => this.átlag = value; }
    }
}
