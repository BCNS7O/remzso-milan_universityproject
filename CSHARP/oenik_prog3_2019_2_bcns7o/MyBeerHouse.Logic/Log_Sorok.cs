﻿// <copyright file="Log_Sorok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;

    /// <summary>
    /// Logic layer class dealing with the Sorok table.
    /// </summary>
    public class Log_Sorok : ILogic<Sörök>
    {
        private IRepository<Sörök> repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Log_Sorok"/> class.
        /// </summary>
        public Log_Sorok()
        {
            this.Repo = new Rep_Sorok();
        }

        /// <summary>
        /// Gets or Sets the repository for the CRUD logic.
        /// </summary>
        public IRepository<Sörök> Repo { get => this.repo; set => this.repo = value; }

        /// <summary>
        /// Calls the repository query select by id.
        /// </summary>
        /// <param name="ezAlapjanKerdez">This is the id.</param>
        /// <returns>Returns a string.</returns>
        public string Lekerdezes_ById(string ezAlapjanKerdez)
        {
            var lekerdezes_ketto = from e in this.Repo.Lekerdezes() select e;
            foreach (var item in lekerdezes_ketto)
            {
                if (item.Név.ToLower() == ezAlapjanKerdez.ToLower())
                {
                    var megoldas = item.Név + " " + item.Ár + " " + item.Típus + " " + item.Alkohol + " " + item.Beosztott_id + " " + item.Glutén;
                    return megoldas;
                }
            }

            return "Nincs ilyen record";
        }

        /// <summary>
        /// Calls the repositry method to update a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        /// <returns>Used for Unit Testing.</returns>
        public bool Frissit(string input, string id, string mitfrissiteni)
        {
            try
            {
                this.Repo.Frissit(input, id, mitfrissiteni);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Calls the repository method to create a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        /// <returns>Used for unit testing.</returns>
        public bool Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam)
        {
            try
            {
                this.Repo.Hozzaad(ujnev, ujnem, ujfizetes, ujkor, ujcsatlakozas, szam);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Calls the whole query of the table from the repository layer.
        /// </summary>
        /// <returns>string.</returns>
        public string Lekerdezes()
        {
            string megoldas = string.Empty;
            var lekerdezes = this.Repo.Lekerdezes();
            foreach (var item in lekerdezes)
            {
                megoldas += item.Név + " " + item.Ár + " " + item.Típus + " " + item.Alkohol + " " + item.Beosztott_id + " " + item.Glutén + "\r\n";
            }

            return megoldas;
        }

        /// <summary>
        /// Calls the record deleting method from the repo layer.
        /// </summary>
        /// <param name="input">This tells which record to delete.</param>
        /// <returns>Used for unit testing.</returns>
        public bool Torol(string input)
        {
            try
            {
                this.Repo.Torol(input);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
