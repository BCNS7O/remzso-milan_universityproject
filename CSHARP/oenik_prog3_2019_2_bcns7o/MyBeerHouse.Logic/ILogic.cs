﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the logic layer(only CRUD).
    /// </summary>
    /// <typeparam name="T">Generic parameter for the different tables.</typeparam>
    public interface ILogic<T>
    {
        /// <summary>
        /// This Queries the whole table.
        /// </summary>
        /// <returns>IQuaryable.</returns>
        string Lekerdezes();

        /// <summary>
        /// Queries the table based on ID.
        /// </summary>
        /// <param name="input">This is the id.</param>
        /// <returns>Returns a string.</returns>
        string Lekerdezes_ById(string input);

        /// <summary>
        /// This creates a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        /// <returns>Used for Unit testing.</returns>
        bool Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam);

        /// <summary>
        /// Deleting a certain record from the table.
        /// </summary>
        /// <param name="input">This tells which recrod to delete.</param>
        /// <returns>Used for Unit testing.</returns>
        bool Torol(string input);

        /// <summary>
        /// Updating a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        /// <returns>Used for Unit testing.</returns>
        bool Frissit(string input, string id, string mitfrissiteni);
    }
}
