﻿// <copyright file="INonCrud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the Non Crud queries in the Logic layer.
    /// </summary>
    public interface INonCrud
    {
        /// <summary>
        /// Queries those employees who has their own beer.
        /// </summary>
        /// <returns>The result of the query.</returns>
        IEnumerable<Q_Menusor> Menusör();

        /// <summary>
        /// Queries the different beers based on type.
        /// </summary>
        /// <returns>The result of the query.</returns>
        IEnumerable<Q_Sortipusokszama> Sörtipusokszama();

        /// <summary>
        /// Queries those employees whose beers are cheaper then 800FT.
        /// </summary>
        /// <returns>The result of the query.</returns>
        IEnumerable<Q_MindenbeosztottAkiSore800FelettVan> MindenbeosztottAkiSore800FelettVan();
    }
}
