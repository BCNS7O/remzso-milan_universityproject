﻿// <copyright file="Log_Java.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// This class controls the Java part of the assigment.
    /// </summary>
    public static class Log_Java
    {
        /// <summary>
        /// This method calls the Java part of the assigment.
        /// </summary>
        /// <returns>Returns a XDoc.</returns>
        public static XDocument JavaVegpont()
        {
            string url = @"http://localhost:8080/oenik_prog3_2019_2_bcns7o/UjReceptServlet";
            using (var kliens = new WebClient())
            {
                try
                {
                    string adat = kliens.DownloadString(url);
                    XDocument doc = XDocument.Parse(adat);
                    return doc;
                }
                catch (WebException e)
                {
                    throw e;
                }
            }
        }
    }
}
