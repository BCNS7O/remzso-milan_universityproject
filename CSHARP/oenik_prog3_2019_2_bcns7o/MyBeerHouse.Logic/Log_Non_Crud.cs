﻿// <copyright file="Log_Non_Crud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;

    /// <summary>
    /// Logic layer class dealing with Not CRUD queries.
    /// </summary>
    public class Log_Non_Crud : INonCrud
    {
        private IRepNonCrud repoNonCrud;

        /// <summary>
        /// Initializes a new instance of the <see cref="Log_Non_Crud"/> class.
        /// </summary>
        public Log_Non_Crud()
        {
            this.RepoNonCrud = new Rep_Non_Crud();
        }

        /// <summary>
        /// Gets or Sets the repository for the Non Crud Queires.
        /// </summary>
        public IRepNonCrud RepoNonCrud { get => this.repoNonCrud; set => this.repoNonCrud = value; }

        /// <summary>
        /// Queries those employees who has their own beer.
        /// </summary>
        /// <returns>The result of the query.</returns>
        public IEnumerable<Q_Menusor> Menusör()
        {
            IEnumerable<Q_Menusor> megoldas = from b in this.RepoNonCrud.LekerBeosztottak()
                                              join s in this.RepoNonCrud.LekerSörök() on b.Beosztott_id equals s.Beosztott_id
                                              join m in this.RepoNonCrud.LekerMenük() on s.Név equals m.Ital
                                              where m.Ár.HasValue
                                              select new Q_Menusor
                                              {
                                                  Név = b.Név,
                                              };

            return megoldas;
        }

        /// <summary>
        /// Queries the different beers based on type.
        /// </summary>
        /// <returns>The result of the query.</returns>
        public IEnumerable<Q_Sortipusokszama> Sörtipusokszama()
        {
            IEnumerable<Q_Sortipusokszama> megoldas = from b in this.RepoNonCrud.LekerBeosztottak()
                                                      join s in this.RepoNonCrud.LekerSörök() on b.Beosztott_id equals s.Beosztott_id
                                                      group s by s.Típus into g
                                                      select new Q_Sortipusokszama
                                                      {
                                                          Név = g.Key,
                                                          Átlag = g.Count(),
                                                      };
            return megoldas;
        }

        /// <summary>
        /// Queries those employees whose beers are cheaper then 800FT.
        /// </summary>
        /// <returns>The result of the query.</returns>
        public IEnumerable<Q_MindenbeosztottAkiSore800FelettVan> MindenbeosztottAkiSore800FelettVan()
        {
            IEnumerable<Q_MindenbeosztottAkiSore800FelettVan> megoldas = from b in this.RepoNonCrud.LekerBeosztottak()
                                                                         join s in this.RepoNonCrud.LekerSörök() on b.Beosztott_id equals s.Beosztott_id
                                                                         where s.Ár < 800
                                                                         select new Q_MindenbeosztottAkiSore800FelettVan
                                                                         {
                                                                             Név = b.Név,
                                                                         };
            return megoldas;
        }
    }
}
