﻿// <copyright file="Q_MindenbeosztottAkiSore800FelettVan.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;

    /// <summary>
    /// This class exists to be the parameter to the generic Non crud interface.
    /// </summary>
    public class Q_MindenbeosztottAkiSore800FelettVan
    {
        private string név;

        /// <summary>
        /// Initializes a new instance of the <see cref="Q_MindenbeosztottAkiSore800FelettVan"/> class.
        /// </summary>
        public Q_MindenbeosztottAkiSore800FelettVan()
        {
        }

        /// <summary>
        /// Gets or sets the name property of the Query.
        /// </summary>
        public string Név { get => this.név; set => this.név = value; }
    }
}
