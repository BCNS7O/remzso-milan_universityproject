﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This is the interface of the repository layer.
    /// </summary>
    /// <typeparam name="T">Generic parameter.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// This Queries the whole table.
        /// </summary>
        /// <returns>IQuaryable.</returns>
        IQueryable<T> Lekerdezes();

        /// <summary>
        /// This creates a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        void Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam);

        /// <summary>
        /// Deleting a certain record from the table.
        /// </summary>
        /// <param name="input">This tells which recrod to delete.</param>
        void Torol(string input);

        /// <summary>
        /// Updating a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        void Frissit(string input, string id, string mitfrissiteni);
    }
}