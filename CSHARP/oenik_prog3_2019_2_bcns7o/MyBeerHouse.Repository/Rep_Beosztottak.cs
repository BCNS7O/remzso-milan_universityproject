﻿// <copyright file="Rep_Beosztottak.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;

    /// <summary>
    /// This controls the repository of the beosztottak table.
    /// </summary>
    public class Rep_Beosztottak : IRepository<Beosztottak>
    {
        private DBEntities tablak;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rep_Beosztottak"/> class.
        /// </summary>
        public Rep_Beosztottak()
        {
            this.tablak = new DBEntities();
        }

        /// <summary>
        /// Updating a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        public void Frissit(string input, string id, string mitfrissiteni)
        {
            switch (mitfrissiteni)
            {
                case "1":
                    Beosztottak beosztott_egy = this.tablak.Beosztottak.Single(x => x.Beosztott_id == id);
                    beosztott_egy.Név = input;
                    this.tablak.SaveChanges();
                    break;
                case "2":
                    Beosztottak beosztott_ketto = this.tablak.Beosztottak.Single(x => x.Beosztott_id == id);
                    beosztott_ketto.Fizetés = Convert.ToInt32(input);
                    this.tablak.SaveChanges();
                    break;
                case "3":
                    Beosztottak beosztott_harom = this.tablak.Beosztottak.Single(x => x.Beosztott_id == id);
                    beosztott_harom.Kor = Convert.ToInt32(input);
                    this.tablak.SaveChanges();
                    break;
                case "4":
                    Beosztottak beosztott_negy = this.tablak.Beosztottak.Single(x => x.Beosztott_id == id);
                    beosztott_negy.Csatlakozás = Convert.ToDateTime(input);
                    this.tablak.SaveChanges();
                    break;
                case "5":
                    Beosztottak beosztott_ot = this.tablak.Beosztottak.Single(x => x.Beosztott_id == id);
                    beosztott_ot.Nem = input;
                    this.tablak.SaveChanges();
                    break;
                default:
                    Console.WriteLine("Iyen menü nem létezik!");
                    System.Threading.Thread.Sleep(800);
                    break;
            }
        }

        /// <summary>
        /// This creates a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        public void Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam)
        {
            Beosztottak ujbeosztott = new Beosztottak
            {
                Név = ujnev,
                Nem = ujnem,
                Fizetés = Convert.ToInt32(ujfizetes),
                Kor = Convert.ToInt32(ujkor),
                Csatlakozás = Convert.ToDateTime(ujcsatlakozas),
                Beosztott_id = szam,
            };
            this.tablak.Beosztottak.Add(ujbeosztott);
            this.tablak.SaveChanges();
        }

        /// <summary>
        /// This Queries the whole table.
        /// </summary>
        /// <returns>IQuaryable.</returns>
        public IQueryable<Beosztottak> Lekerdezes()
        {
            return from e in this.tablak.Beosztottak select e;
        }

        /// <summary>
        /// Deleting a certain record from the table.
        /// </summary>
        /// <param name="input">This tells which record to delete.</param>
        public void Torol(string input)
        {
            var seged = this.tablak.Beosztottak.Where(x => x.Név == input);
            foreach (var item in seged)
            {
                if (item.Név == input)
                {
                    Beosztottak beosztott = this.tablak.Beosztottak.Single(x => x.Név == input);
                    this.tablak.Beosztottak.Remove(beosztott);
                }
                else
                {
                    break;
                }
            }

            this.tablak.SaveChanges();
        }
    }
}
