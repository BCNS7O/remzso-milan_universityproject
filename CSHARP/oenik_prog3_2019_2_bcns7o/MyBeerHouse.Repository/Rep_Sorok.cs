﻿// <copyright file="Rep_Sorok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;

    /// <summary>
    /// This controls the repository of the sorok table.
    /// </summary>
    public class Rep_Sorok : IRepository<Sörök>
    {
        private readonly DBEntities tablak;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rep_Sorok"/> class.
        /// </summary>
        public Rep_Sorok()
        {
            this.tablak = new DBEntities();
        }

        /// <summary>
        /// Updating a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        public void Frissit(string input, string id, string mitfrissiteni)
        {
            switch (mitfrissiteni)
            {
                case "1":
                    Sörök sor_egy = this.tablak.Sörök.Single(x => x.Név == id);
                    sor_egy.Ár = Convert.ToInt32(input);
                    this.tablak.SaveChanges();
                    break;
                case "2":
                    Sörök sor_ketto = this.tablak.Sörök.Single(x => x.Név == id);
                    sor_ketto.Típus = input;
                    this.tablak.SaveChanges();
                    break;
                case "3":
                    Sörök sor_harom = this.tablak.Sörök.Single(x => x.Név == id);
                    sor_harom.Alkohol = Convert.ToInt32(input);
                    this.tablak.SaveChanges();
                    break;
                case "4":
                    Sörök sor_negy = this.tablak.Sörök.Single(x => x.Név == id);
                    sor_negy.Glutén = input;
                    this.tablak.SaveChanges();
                    break;
                default:
                    Console.WriteLine("Iyen menü nem létezik!");
                    System.Threading.Thread.Sleep(800);
                    break;
            }
        }

        /// <summary>
        /// This creates a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        public void Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam)
        {
            Sörök ujSörök = new Sörök
            {
                Név = ujnev,
                Ár = Convert.ToInt32(ujnem),
                Típus = ujfizetes,
                Alkohol = Convert.ToInt32(ujkor),
                Beosztott_id = ujcsatlakozas,
                Glutén = szam,
            };
            this.tablak.Sörök.Add(ujSörök);
            this.tablak.SaveChanges();
        }

        /// <summary>
        /// This Queries the whole table.
        /// </summary>
        /// <returns>IQuaryable.</returns>
        public IQueryable<Sörök> Lekerdezes()
        {
            return from e in this.tablak.Sörök select e;
        }

        /// <summary>
        /// Deleting a certain record from the table.
        /// </summary>
        /// <param name="input">This tells which recrod to delete.</param>
        public void Torol(string input)
        {
            var seged = this.tablak.Sörök.Where(x => x.Név == input);
            foreach (var item in seged)
            {
                if (item.Név == input)
                {
                    Sörök sor = this.tablak.Sörök.Single(x => x.Név == input);
                    this.tablak.Sörök.Remove(sor);
                }
                else
                {
                    break;
                }
            }

            this.tablak.SaveChanges();
        }
    }
}
