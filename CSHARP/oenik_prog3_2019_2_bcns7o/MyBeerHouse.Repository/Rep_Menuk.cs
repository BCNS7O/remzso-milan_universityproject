﻿// <copyright file="Rep_Menuk.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;

    /// <summary>
    /// This controls the repository of the menuk table.
    /// </summary>
    public class Rep_Menuk : IRepository<Menük>
    {
        private readonly DBEntities tablak;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rep_Menuk"/> class.
        /// </summary>
        public Rep_Menuk()
        {
            this.tablak = new DBEntities();
        }

        /// <summary>
        /// Updating a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        public void Frissit(string input, string id, string mitfrissiteni)
        {
            Menük menu = this.tablak.Menük.Single(x => x.Név == id);
            menu.Ár = Convert.ToInt32(input);
            this.tablak.SaveChanges();
        }

        /// <summary>
        /// This creates a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        public void Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam)
        {
            Menük ujmenük = new Menük
            {
                Név = ujnev,
                Ár = Convert.ToInt32(ujnem),
                Étel = ujfizetes,
                Ital = ujkor,
            };
            this.tablak.Menük.Add(ujmenük);
            this.tablak.SaveChanges();
        }

        /// <summary>
        /// This Queries the whole table.
        /// </summary>
        /// <returns>IQuaryable.</returns>
        public IQueryable<Menük> Lekerdezes()
        {
            return from e in this.tablak.Menük select e;
        }

        /// <summary>
        /// Deleting a certain record from the table.
        /// </summary>
        /// <param name="input">This tells which recrod to delete.</param>
        public void Torol(string input)
        {
            var seged = this.tablak.Menük.Where(x => x.Név == input);
            foreach (var item in seged)
            {
                if (item.Név == input)
                {
                    Menük menu = this.tablak.Menük.Single(x => x.Név == input);
                    this.tablak.Menük.Remove(menu);
                }
                else
                {
                    break;
                }
            }

            this.tablak.SaveChanges();
        }
    }
}
