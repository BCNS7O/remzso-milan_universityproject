﻿// <copyright file="Rep_Etelek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;

    /// <summary>
    /// This controls the repository of the etelek table.
    /// </summary>
    public class Rep_Etelek : IRepository<Ételek>
    {
        private readonly DBEntities tablak;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rep_Etelek"/> class.
        /// </summary>
        public Rep_Etelek()
        {
            this.tablak = new DBEntities();
        }

        /// <summary>
        /// Updating a record.
        /// </summary>
        /// <param name="input">This is the new information.</param>
        /// <param name="id">This is the id of the record that needs updating.</param>
        /// <param name="mitfrissiteni">This determines the column that will be updated.</param>
        public void Frissit(string input, string id, string mitfrissiteni)
        {
            switch (mitfrissiteni)
            {
                case "1":
                    Ételek etel_egy = this.tablak.Ételek.Single(x => x.Név == id);
                    etel_egy.Ár = Convert.ToInt32(input);
                    this.tablak.SaveChanges();
                    break;
                case "2":
                    Ételek etel_ketto = this.tablak.Ételek.Single(x => x.Név == id);
                    etel_ketto.Vegetáriánus = input;
                    this.tablak.SaveChanges();
                    break;
                case "3":
                    Ételek etel_harom = this.tablak.Ételek.Single(x => x.Név == id);
                    etel_harom.Csipős = input;
                    this.tablak.SaveChanges();
                    break;
                case "4":
                    Ételek etel_negy = this.tablak.Ételek.Single(x => x.Név == id);
                    etel_negy.Glutén = input;
                    this.tablak.SaveChanges();
                    break;
                case "5":
                    Ételek etel_ot = this.tablak.Ételek.Single(x => x.Név == id);
                    etel_ot.DiákKedvezmény = input;
                    this.tablak.SaveChanges();
                    break;
                default:
                    Console.WriteLine("Iyen menü nem létezik!");
                    System.Threading.Thread.Sleep(800);
                    break;
            }
        }

        /// <summary>
        /// This creates a new record for the table.
        /// </summary>
        /// <param name="ujnev">A column of the table.</param>
        /// <param name="ujnem">Another column of the table.</param>
        /// <param name="ujfizetes">Just another column of the table.</param>
        /// <param name="ujkor">A parameter.</param>
        /// <param name="ujcsatlakozas">Another parameter.</param>
        /// <param name="szam">Even more parameter.</param>
        public void Hozzaad(string ujnev, string ujnem, string ujfizetes, string ujkor, string ujcsatlakozas, string szam)
        {
            Ételek ujÉtelek = new Ételek
            {
                Név = ujnev,
                Ár = Convert.ToInt32(ujnem),
                Vegetáriánus = ujfizetes,
                Csipős = ujkor,
                Glutén = ujcsatlakozas,
                DiákKedvezmény = szam,
            };
            this.tablak.Ételek.Add(ujÉtelek);
            this.tablak.SaveChanges();
        }

        /// <summary>
        /// This Queries the whole table.
        /// </summary>
        /// <returns>IQuaryable.</returns>
        public IQueryable<Ételek> Lekerdezes()
        {
            return from e in this.tablak.Ételek select e;
        }

        /// <summary>
        /// Deleting a certain record from the table.
        /// </summary>
        /// <param name="input">This tells which recrod to delete.</param>
        public void Torol(string input)
        {
            var seged = this.tablak.Ételek.Where(x => x.Név == input);
            foreach (var item in seged)
            {
                if (item.Név == input)
                {
                    Ételek etel = this.tablak.Ételek.Single(x => x.Név == input);
                    this.tablak.Ételek.Remove(etel);
                }
                else
                {
                    break;
                }
            }

            this.tablak.SaveChanges();
        }
    }
}
