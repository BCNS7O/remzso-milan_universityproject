﻿// <copyright file="Rep_Non_Crud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;

    /// <summary>
    /// The class the controls the not crud queries's repository.
    /// </summary>
    public class Rep_Non_Crud : IRepNonCrud
    {
        private readonly DBEntities tablak;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rep_Non_Crud"/> class.
        /// </summary>
        public Rep_Non_Crud()
        {
            this.tablak = new DBEntities();
        }

        /// <summary>
        /// Queries the beosztottak table.
        /// </summary>
        /// <returns>The beosztottak table.</returns>
        public IQueryable<Beosztottak> LekerBeosztottak()
        {
            return from e in this.tablak.Beosztottak select e;
        }

        /// <summary>
        /// Queries the etelek table.
        /// </summary>
        /// <returns>The etelek table.</returns>
        public IQueryable<Ételek> LekerÉtelek()
        {
            return from e in this.tablak.Ételek select e;
        }

        /// <summary>
        /// Queries the menuk table.
        /// </summary>
        /// <returns>The menuk table.</returns>
        public IQueryable<Menük> LekerMenük()
        {
            return from e in this.tablak.Menük select e;
        }

        /// <summary>
        /// Queries the sorok table.
        /// </summary>
        /// <returns>The sorok table.</returns>
        public IQueryable<Sörök> LekerSörök()
        {
            return from e in this.tablak.Sörök select e;
        }
    }
}
