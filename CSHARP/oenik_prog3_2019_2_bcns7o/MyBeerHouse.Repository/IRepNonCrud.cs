﻿// <copyright file="IRepNonCrud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Data;

    /// <summary>
    /// Inerface for the Non CRUD repository.
    /// </summary>
    public interface IRepNonCrud
    {
        /// <summary>
        /// Gets the Beosztottak table from the Data layer.
        /// </summary>
        /// <returns>Returns the whole table.</returns>
        IQueryable<Beosztottak> LekerBeosztottak();

        /// <summary>
        /// Gets the Ételek table from the Data layer.
        /// </summary>
        /// <returns>Returns the whole table.</returns>
        IQueryable<Ételek> LekerÉtelek();

        /// <summary>
        /// Gets the Menük table from the Data layer.
        /// </summary>
        /// <returns>Returns the whole table.</returns>
        IQueryable<Menük> LekerMenük();

        /// <summary>
        /// Gets the Sörök table from the Data layer.
        /// </summary>
        /// <returns>Returns the whole table.</returns>
        IQueryable<Sörök> LekerSörök();
    }
}
