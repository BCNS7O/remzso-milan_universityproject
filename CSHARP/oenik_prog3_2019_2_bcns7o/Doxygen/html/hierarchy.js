var hierarchy =
[
    [ "MyBeerHouse.Data.Beosztottak", "class_my_beer_house_1_1_data_1_1_beosztottak.html", null ],
    [ "DbContext", null, [
      [ "MyBeerHouse.Data.DBEntities", "class_my_beer_house_1_1_data_1_1_d_b_entities.html", null ]
    ] ],
    [ "MyBeerHouse.Logic.ILogic< T >", "interface_my_beer_house_1_1_logic_1_1_i_logic.html", null ],
    [ "MyBeerHouse.Logic.ILogic< Beosztottak >", "interface_my_beer_house_1_1_logic_1_1_i_logic.html", [
      [ "MyBeerHouse.Logic.Log_Beosztottak", "class_my_beer_house_1_1_logic_1_1_log___beosztottak.html", null ]
    ] ],
    [ "MyBeerHouse.Logic.ILogic< Menük >", "interface_my_beer_house_1_1_logic_1_1_i_logic.html", [
      [ "MyBeerHouse.Logic.Log_Menuk", "class_my_beer_house_1_1_logic_1_1_log___menuk.html", null ]
    ] ],
    [ "MyBeerHouse.Logic.ILogic< Sörök >", "interface_my_beer_house_1_1_logic_1_1_i_logic.html", [
      [ "MyBeerHouse.Logic.Log_Sorok", "class_my_beer_house_1_1_logic_1_1_log___sorok.html", null ]
    ] ],
    [ "MyBeerHouse.Logic.ILogic< Ételek >", "interface_my_beer_house_1_1_logic_1_1_i_logic.html", [
      [ "MyBeerHouse.Logic.Log_Etelek", "class_my_beer_house_1_1_logic_1_1_log___etelek.html", null ]
    ] ],
    [ "MyBeerHouse.Logic.INonCrud", "interface_my_beer_house_1_1_logic_1_1_i_non_crud.html", [
      [ "MyBeerHouse.Logic.Log_Non_Crud", "class_my_beer_house_1_1_logic_1_1_log___non___crud.html", null ]
    ] ],
    [ "MyBeerHouse.Repository.IRepNonCrud", "interface_my_beer_house_1_1_repository_1_1_i_rep_non_crud.html", [
      [ "MyBeerHouse.Repository.Rep_Non_Crud", "class_my_beer_house_1_1_repository_1_1_rep___non___crud.html", null ]
    ] ],
    [ "MyBeerHouse.Repository.IRepository< T >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", null ],
    [ "MyBeerHouse.Repository.IRepository< Beosztottak >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", [
      [ "MyBeerHouse.Repository.Rep_Beosztottak", "class_my_beer_house_1_1_repository_1_1_rep___beosztottak.html", null ]
    ] ],
    [ "MyBeerHouse.Repository.IRepository< Menük >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", [
      [ "MyBeerHouse.Repository.Rep_Menuk", "class_my_beer_house_1_1_repository_1_1_rep___menuk.html", null ]
    ] ],
    [ "MyBeerHouse.Repository.IRepository< MyBeerHouse.Data.Beosztottak >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", null ],
    [ "MyBeerHouse.Repository.IRepository< MyBeerHouse.Data.Menük >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", null ],
    [ "MyBeerHouse.Repository.IRepository< MyBeerHouse.Data.Sörök >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", null ],
    [ "MyBeerHouse.Repository.IRepository< MyBeerHouse.Data.Ételek >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", null ],
    [ "MyBeerHouse.Repository.IRepository< Sörök >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", [
      [ "MyBeerHouse.Repository.Rep_Sorok", "class_my_beer_house_1_1_repository_1_1_rep___sorok.html", null ]
    ] ],
    [ "MyBeerHouse.Repository.IRepository< Ételek >", "interface_my_beer_house_1_1_repository_1_1_i_repository.html", [
      [ "MyBeerHouse.Repository.Rep_Etelek", "class_my_beer_house_1_1_repository_1_1_rep___etelek.html", null ]
    ] ],
    [ "MyBeerHouse.Data.Menük", "class_my_beer_house_1_1_data_1_1_men_xC3_xBCk.html", null ],
    [ "MyBeerHouse.Logic.Q_Menusor", "class_my_beer_house_1_1_logic_1_1_q___menusor.html", null ],
    [ "MyBeerHouse.Logic.Q_MindenbeosztottAkiSore800FelettVan", "class_my_beer_house_1_1_logic_1_1_q___mindenbeosztott_aki_sore800_felett_van.html", null ],
    [ "MyBeerHouse.Logic.Q_Sortipusokszama", "class_my_beer_house_1_1_logic_1_1_q___sortipusokszama.html", null ],
    [ "MyBeerHouse.Data.Sörök", "class_my_beer_house_1_1_data_1_1_s_xC3_xB6r_xC3_xB6k.html", null ],
    [ "MyBeerHouse.Logic.Tests.Test_Beosztottak", "class_my_beer_house_1_1_logic_1_1_tests_1_1_test___beosztottak.html", null ],
    [ "MyBeerHouse.Logic.Tests.Test_Etelek", "class_my_beer_house_1_1_logic_1_1_tests_1_1_test___etelek.html", null ],
    [ "MyBeerHouse.Logic.Tests.Test_Menuk", "class_my_beer_house_1_1_logic_1_1_tests_1_1_test___menuk.html", null ],
    [ "MyBeerHouse.Logic.Tests.Test_Sorok", "class_my_beer_house_1_1_logic_1_1_tests_1_1_test___sorok.html", null ],
    [ "MyBeerHouse.Data.Ételek", "class_my_beer_house_1_1_data_1_1_xC3_x89telek.html", null ]
];