var namespace_my_beer_house_1_1_logic =
[
    [ "Tests", "namespace_my_beer_house_1_1_logic_1_1_tests.html", "namespace_my_beer_house_1_1_logic_1_1_tests" ],
    [ "ILogic", "interface_my_beer_house_1_1_logic_1_1_i_logic.html", "interface_my_beer_house_1_1_logic_1_1_i_logic" ],
    [ "INonCrud", "interface_my_beer_house_1_1_logic_1_1_i_non_crud.html", "interface_my_beer_house_1_1_logic_1_1_i_non_crud" ],
    [ "Log_Beosztottak", "class_my_beer_house_1_1_logic_1_1_log___beosztottak.html", "class_my_beer_house_1_1_logic_1_1_log___beosztottak" ],
    [ "Log_Etelek", "class_my_beer_house_1_1_logic_1_1_log___etelek.html", "class_my_beer_house_1_1_logic_1_1_log___etelek" ],
    [ "Log_Menuk", "class_my_beer_house_1_1_logic_1_1_log___menuk.html", "class_my_beer_house_1_1_logic_1_1_log___menuk" ],
    [ "Log_Non_Crud", "class_my_beer_house_1_1_logic_1_1_log___non___crud.html", "class_my_beer_house_1_1_logic_1_1_log___non___crud" ],
    [ "Log_Sorok", "class_my_beer_house_1_1_logic_1_1_log___sorok.html", "class_my_beer_house_1_1_logic_1_1_log___sorok" ],
    [ "Q_Menusor", "class_my_beer_house_1_1_logic_1_1_q___menusor.html", "class_my_beer_house_1_1_logic_1_1_q___menusor" ],
    [ "Q_MindenbeosztottAkiSore800FelettVan", "class_my_beer_house_1_1_logic_1_1_q___mindenbeosztott_aki_sore800_felett_van.html", "class_my_beer_house_1_1_logic_1_1_q___mindenbeosztott_aki_sore800_felett_van" ],
    [ "Q_Sortipusokszama", "class_my_beer_house_1_1_logic_1_1_q___sortipusokszama.html", "class_my_beer_house_1_1_logic_1_1_q___sortipusokszama" ]
];