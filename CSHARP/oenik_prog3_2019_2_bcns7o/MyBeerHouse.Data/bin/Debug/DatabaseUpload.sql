﻿Create Table  Beosztottak(
Név VARCHAR(40),
Nem VARCHAR(40),
 Fizetés INT,
 Kor INT,
  Csatlakozás DATE,
 Beosztott_id VARCHAR(40),
Constraint pk_Beosztott_id
Primary Key(Beosztott_id));

Create Table  Ételek(
Név VARCHAR(40),
 Ár INT,
 Vegetáriánus VARCHAR(40),
 Csipős VARCHAR(40),
 Glutén VARCHAR(40),
 DiákKedvezmény VARCHAR(40),
Constraint pk_ételek_név
Primary Key(név));

DROP TABLE Beosztottak;

DROP TABLE Ételek;

DROP TABLE Sörök;

DROP TABLE Menük;

Create Table  Sörök(
Név VARCHAR(40),
 Ár INT,
 Típus VARCHAR(40),
 Alkohol INT,
 Beosztott_id VARCHAR(40),
 Glutén VARCHAR(40),
Constraint fk_Beosztott_id
Foreign Key(Beosztott_id)
References beosztottak (Beosztott_id)  on delete cascade,
Constraint pk_sörök_név
Primary Key(név));


Create Table  Menük(
Név VARCHAR(40),
Ár INT,
 Étel VARCHAR(40),
 Ital VARCHAR(40),
Constraint fk_Étel
Foreign Key(Étel)
References ételek (Név),
Constraint fk_Ital
Foreign Key(Ital) 
References sörök (Név) on delete cascade,
 Constraint pk_menü_név
Primary Key(név));

INSERT INTO Beosztottak
VALUES('Keményfi Koppány Dávid','Férfi',290000, 40, convert(DATETIME,'1994.06.04'), 42);
INSERT INTO Beosztottak
VALUES('Tóth Kristóf László','Férfi',270000, 29, convert(DATETIME,'2011.08.14'), 69);
INSERT INTO Beosztottak
VALUES('Vona Csaba', 'Férfi',360000,86, convert(DATETIME,'2017.01.23'), 1);
INSERT INTO Beosztottak
VALUES('Alexander Nikolayov','Férfi',180000, 80, convert(DATETIME,'2011.12.24'), 11);
INSERT INTO Beosztottak
VALUES('Dave Bubo','Férfi',300000, 56, convert(DATETIME,'2007.02.13'), 66);
INSERT INTO Beosztottak
VALUES('Braun Nikoletta','Nő',260000, 24, convert(DATETIME,'2016.01.01'), 4);
INSERT INTO Beosztottak
VALUES('Romanov András','Férfi',260000, 45, convert(DATETIME,'2002.5.04'), 32);
INSERT INTO Beosztottak
VALUES('Kullenberg Péter','Férfi',290000, 43, convert(DATETIME,'1996.7.02'), 3);
INSERT INTO Beosztottak
VALUES('Börzsönyi Kirstóf','Férfi',290000, 32, convert(DATETIME,'2010.8.23'), 30);
INSERT INTO Beosztottak
VALUES('Nemecsek Artyom','Férfi',260000, 26, convert(DATETIME,'2018.3.26'), 50);
INSERT INTO Beosztottak
VALUES('Kovács Réka','Nő',180000, 72, convert(DATETIME,'2010.9.11'), 70);


INSERT INTO Ételek
VALUES('Melegszendvics', 520,'Nem','Igen','Igen','Igen');
INSERT INTO Ételek
VALUES('Csirkemell sajtmártásban, rizzsel', 1430,'Nem','Nem','Nem','Nem');
INSERT INTO Ételek
VALUES('Hortobágyi Húsos Palacsinta', 1200,'Nem','Nem','Igen','Igen');
INSERT INTO Ételek
VALUES('Miccs', 1000,'Nem','Igen','Nem','Nem');
INSERT INTO Ételek
VALUES('Mákos tészta', 700,'Igen','Nem','Igen','Igen');
INSERT INTO Ételek
VALUES('Finom leves', 1200,'Igen','Nem','Nem','Igen');
INSERT INTO Ételek
VALUES('Rétes', 500,'Igen','Nem','Igen','Igen');
INSERT INTO Ételek
VALUES('Sültkrupmli', 1000,'Igen','Nem','Nem','Igen');
INSERT INTO Ételek
VALUES('Zsiros Kenyér', 250,'Nem','Igen','Igen','Igen');

INSERT INTO Sörök
VALUES('Oroszlán Lehelete', 630, 'Lager',4.65, 42,'Igen');
INSERT INTO SÖRÖK
VALUES('Burnout', 510, 'APA' ,11.5, 69,'Igen');
INSERT INTO Sörök
VALUES('Ratass ', 850, 'Lager' ,4.65, 1,'Igen');
INSERT INTO Sörök
VALUES('Fekete Delfin', 690, 'STOUT' ,4.65, 11,'Igen');
INSERT INTO Sörök
VALUES('Keserü Üstökös', 510, 'IPA' ,4.65, 66,'Nem');
INSERT INTO Sörök
VALUES('Atom Kolmós', 690, 'IPA' ,6.65, 66,'Igen');
INSERT INTO Sörök
VALUES('Nyári szél', 500, 'Lager' ,2.65 ,50,'Nem');
INSERT INTO Sörök
VALUES('Akció mobil', 900, 'Barna' ,4.65, 4,'Igen');
INSERT INTO Sörök
VALUES('Hegyi folyam', 600, 'APA' ,2.65, 1,'Nem');
INSERT INTO Sörök
VALUES('Olvadó Hó', 400, 'Lager' ,4.65, 32,'Igen');
INSERT INTO Sörök
VALUES('Mélység', 920, 'Barna' ,11.5, 30,'Igen');

INSERT INTO Menük
VALUES('Oroszlán Étke', 1790, 'Csirkemell sajtmártásban, rizzsel' , 'Oroszlán Lehelete');
INSERT INTO Menük
VALUES('Miccs-out', 1200, 'Miccs' , 'Burnout');
INSERT INTO Menük
VALUES('Chillzone', 1100, 'Zsiros Kenyér' , 'Olvadó Hó');
INSERT INTO Menük
VALUES('Éhségüzö', 1600, 'Hortobágyi Húsos Palacsinta' , 'Mélység');
INSERT INTO Menük
VALUES('Finom_egyed', 1000, 'Finom leves' , 'Keserü Üstökös');
INSERT INTO Menük
VALUES('Megéri', 1800, 'Mákos tészta' , 'Akció mobil');