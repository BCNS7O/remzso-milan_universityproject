﻿// <copyright file="Con_Java.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_bcns7o
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using MyBeerHouse.Logic;

    /// <summary>
    /// Gets the data from the Java part of the assignment and writes it to console.
    /// </summary>
    internal static class Con_Java
    {
        /// <summary>
        /// Writes to console.
        /// </summary>
        public static void JavaKiir()
        {
            XDocument doc = Log_Java.JavaVegpont();
            foreach (var item in doc.Root.Descendants("Ujsorok"))
            {
                Console.WriteLine("Név: " + item.Element("nev").Value + " Ár: " + item.Element("ar").Value + " Tipus: " + item.Element("tipus").Value + " Alkohol: " + item.Element("alkohol").Value + " Glutén: " + item.Element("gluten").Value);
            }

            Console.WriteLine(Environment.NewLine + "Enter hogy vissza menj");
            Console.ReadLine();
        }
    }
}
