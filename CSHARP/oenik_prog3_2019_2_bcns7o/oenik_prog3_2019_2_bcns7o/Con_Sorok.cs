﻿// <copyright file="Con_Sorok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_bcns7o
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Logic;

    /// <summary>
    /// Deals with the console layer of the Beosztottak table.
    /// Like Writing to the console, and menus.
    /// </summary>
    internal class Con_Sorok
    {
        private readonly Log_Sorok logi;

        /// <summary>
        /// Initializes a new instance of the <see cref="Con_Sorok"/> class.
        /// </summary>
        public Con_Sorok()
        {
            this.logi = new Log_Sorok();
        }

        /// <summary>
        /// Controls the menu of the update function.
        /// </summary>
        public void Frissit()
        {
            Console.WriteLine("A sörök tábla frissitése, írd be a sör nevét akit frissiteni akarsz: ");
            string id = Console.ReadLine();
            Console.WriteLine("1. Ár");
            Console.WriteLine("2. Tipus");
            Console.WriteLine("3. Alkohol");
            Console.WriteLine("4. Glutén");
            Console.WriteLine("Mit akarsz frissiteni?");
            string mitfrissiteni = Console.ReadLine();
            switch (mitfrissiteni)
            {
                case "1":
                    Console.WriteLine("Add az új árat: ");
                    string input_egy = Console.ReadLine();
                    this.logi.Frissit(input_egy, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "2":
                    Console.WriteLine("Add az új tipust: ");
                    string input_ketto = Console.ReadLine();
                    this.logi.Frissit(input_ketto, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "3":
                    Console.WriteLine("Add az új alkohol tartalmat: ");
                    string input_harom = Console.ReadLine();
                    this.logi.Frissit(input_harom, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "4":
                    Console.WriteLine("Gluténes-e?: ");
                    string input_negy = Console.ReadLine();
                    this.logi.Frissit(input_negy, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                default:
                    Console.WriteLine("Iyen menü nem létezik!");
                    System.Threading.Thread.Sleep(800);
                    break;
            }
        }

        /// <summary>
        /// Controls the menu of the create function.
        /// </summary>
        public void Hozzaad()
        {
            Console.WriteLine("Add meg az új rekord adatait ebben a sorrendben: Név, Ár, Típus, Alkohol, Beosztott_ID, Glutén");
            string ujnev = Console.ReadLine();
            string ujnem = Console.ReadLine();
            string ujfizetes = Console.ReadLine();
            string ujkor = Console.ReadLine();
            string ujcsatlakozas = Console.ReadLine();
            string ujbeoszott_id = Console.ReadLine();
            this.logi.Hozzaad(ujnev, ujnem, ujfizetes, ujkor, ujcsatlakozas, ujbeoszott_id);
        }

        /// <summary>
        /// Controls the menu of the read function.
        /// </summary>
        public void Lekerdezes()
        {
            Console.WriteLine("1. Az egész táblát");
            Console.WriteLine("2. Név szerint.");
            Console.WriteLine("3. Vissza");
            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    Console.WriteLine("Sörök: ");
                    Console.WriteLine("Név, Ár, Típus, Alkohol, Beosztott_ID, Glutén");
                    string kiir = this.logi.Lekerdezes();
                    Console.WriteLine(kiir);

                    break;
                case "2":
                    Console.WriteLine("Írd be a keresett record nevét:");
                    string ezAlapjanKerdez = Console.ReadLine();
                    Console.WriteLine(ezAlapjanKerdez + ": ");
                    Console.WriteLine("Név, Ár, Típus, Alkohol, Beosztott_ID, Glutén");
                    string megoldas = this.logi.Lekerdezes_ById(ezAlapjanKerdez);
                    Console.WriteLine(megoldas);

                    break;
                case "3":
                    break;
                default:
                    Console.WriteLine("Ilyen menü nincs.");
                    break;
            }
        }

        /// <summary>
        /// Controls the menu of the delete function.
        /// </summary>
        public void Torol()
        {
            Console.WriteLine("Táblából törlés, add meg a nevét a sornak, amit törölni akarsz:  ");
            string id = Console.ReadLine();
            this.logi.Torol(id);
            Console.WriteLine("Tábla Frissitve");
        }
    }
}
