﻿// <copyright file="Con_Beosztottak.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_bcns7o
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Logic;

    /// <summary>
    /// Deals with the console layer of the Beosztottak table.
    /// Like Writing to the console, and menus.
    /// </summary>
    internal class Con_Beosztottak
    {
        private readonly Log_Beosztottak logi;

        /// <summary>
        /// Initializes a new instance of the <see cref="Con_Beosztottak"/> class.
        /// </summary>
        public Con_Beosztottak()
        {
            this.logi = new Log_Beosztottak();
        }

        /// <summary>
        /// Controls the menu of the update function.
        /// </summary>
        public void Frissit()
        {
            Console.WriteLine("A beoszottak tábla frissitése, írd be a beosztott ID-ját akit frissiteni akarsz: ");
            string id = Console.ReadLine();
            Console.WriteLine("1. Név");
            Console.WriteLine("2. Fizetés");
            Console.WriteLine("3. Kor");
            Console.WriteLine("4. Csatlakozás");
            Console.WriteLine("5. Nem");
            Console.WriteLine("Mit akarsz frissiteni?");
            string mitfrissiteni = Console.ReadLine();
            switch (mitfrissiteni)
            {
                case "1":
                    Console.WriteLine("Add az új nevet: ");
                    string input_egy = Console.ReadLine();
                    this.logi.Frissit(input_egy, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "2":
                    Console.WriteLine("Add az új fizetést: ");
                    string input_ketto = Console.ReadLine();
                    this.logi.Frissit(input_ketto, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "3":
                    Console.WriteLine("Add az új kort: ");
                    string input_harom = Console.ReadLine();
                    this.logi.Frissit(input_harom, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "4":
                    Console.WriteLine("Add az új csatlakozást: ");
                    string input_negy = Console.ReadLine();
                    this.logi.Frissit(input_negy, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                case "5":
                    Console.WriteLine("Add az új nemet: ");
                    string input_ot = Console.ReadLine();
                    this.logi.Frissit(input_ot, id, mitfrissiteni);
                    Console.WriteLine("Tábla Frissitve");
                    break;
                default:
                    Console.WriteLine("Iyen menü nem létezik!");
                    System.Threading.Thread.Sleep(800);
                    break;
            }
        }

        /// <summary>
        /// Controls the menu of the create function.
        /// </summary>
        public void Hozzaad()
        {
            Console.WriteLine("Add meg az új rekord adatait ebben a sorrendben: Név, Nem, Fizetés, Kor, Csatlakozás, ID");
            string ujnev = Console.ReadLine();
            string ujnem = Console.ReadLine();
            string ujfizetes = Console.ReadLine();
            string ujkor = Console.ReadLine();
            string ujcsatlakozas = Console.ReadLine();
            string ujbeoszott_id = Console.ReadLine();
            this.logi.Hozzaad(ujnev, ujnem, ujfizetes, ujkor, ujcsatlakozas, ujbeoszott_id);
        }

        /// <summary>
        /// Controls the menu of the read function.
        /// </summary>
        public void Lekerdezes()
        {
            Console.WriteLine("1. Az egész táblát");
            Console.WriteLine("2. Név szerint.");
            Console.WriteLine("3. Vissza");
            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    Console.WriteLine("Beosztottak: ");
                    Console.WriteLine("Név, Nem, Fizetés, Kor, Csatlakozás, ID");
                    string kiir = this.logi.Lekerdezes();
                    Console.WriteLine(kiir);

                    break;
                case "2":
                    Console.WriteLine("Írd be a keresett record nevét:");
                    string ezAlapjanKerdez = Console.ReadLine();
                    Console.WriteLine(ezAlapjanKerdez + ": ");
                    Console.WriteLine("Név, Nem, Fizetés, Kor, Csatlakozás, ID");
                    string megoldas = this.logi.Lekerdezes_ById(ezAlapjanKerdez);
                    Console.WriteLine(megoldas);

                    break;
                case "3":
                    break;

                default:
                    Console.WriteLine("Ilyen menü nincs.");
                    break;
            }
        }

        /// <summary>
        /// Controls the menu of the delete function.
        /// </summary>
        public void Torol()
        {
            Console.WriteLine("Táblából törlés, add meg a nevét a sornak, amit törölni akarsz:  ");
            string id = Console.ReadLine();
            this.logi.Torol(id);
            Console.WriteLine("Tábla Frissitve");
        }
    }
}
