﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_bcns7o
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using MyBeerHouse.Logic;

    /// <summary>
    /// This is the starting point of the program.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            bool menu_ciklus = true;
            while (menu_ciklus)
            {
                Console.Clear();
                Console.WriteLine("Remzső Milán, BCNS7O Féléves Feladat: ");
                Console.WriteLine("///////////////////////////////////////");
                Console.WriteLine("1. Táblázatok");
                Console.WriteLine("2. Lekédezések");
                Console.WriteLine("3. Kérek két új sör receptet (Java végpont)");
                Console.WriteLine("4. Kilépés");
                Console.WriteLine("///////////////////////////////////////");
                Console.Write("Válasz számot: ");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        Menu_Tablazatok();
                        break;
                    case "2":
                        Menu_Lekerdezesek();
                        break;
                    case "3":
                        Con_Java.JavaKiir();
                        break;
                    case "4":
                        menu_ciklus = false;
                        break;
                    default:
                        Console.WriteLine("Iyen menü nem létezik!");
                        System.Threading.Thread.Sleep(800);
                        break;
                }
            }
        }

        private static void Menu_Tablazatok()
        {
            string input;
            bool menu_ciklus = true;
            while (menu_ciklus)
            {
                Console.Clear();
                Console.WriteLine("Remzső Milán, BCNS7O Féléves Feladat: ");
                Console.WriteLine("///////////////////////////////////////");
                Console.WriteLine("TÁBLÁZATOK: ");
                Console.WriteLine("1. Beosztottak");
                Console.WriteLine("2. Ételek");
                Console.WriteLine("3. Menük");
                Console.WriteLine("4. Sörök");
                Console.WriteLine("5. Vissza");
                Console.WriteLine("///////////////////////////////////////");
                Console.Write("Válasz számot: ");
                input = Console.ReadLine();
                if (Convert.ToInt32(input) >= 1 && Convert.ToInt32(input) <= 4)
                {
                    Menu_CRUD(input);
                }
                else if (input == "5")
                {
                    menu_ciklus = false;
                }
                else
                {
                    Console.WriteLine("Iyen menü nem létezik!");
                    System.Threading.Thread.Sleep(800);
                }
            }
        }

        private static void Menu_CRUD(string input)
        {
            Con_Beosztottak beosztott = new Con_Beosztottak();
            Con_Etelek etel = new Con_Etelek();
            Con_Sorok sor = new Con_Sorok();
            Con_Menuk menu = new Con_Menuk();

            Console.Clear();
            bool menu_ciklus = true;
            string tablazat = string.Empty;
            switch (input)
            {
                case "1":
                    tablazat = "Beosztottak";
                    break;
                case "2":
                    tablazat = "Ételek";
                    break;
                case "3":
                    tablazat = "Menük";
                    break;
                case "4":
                    tablazat = "Sörök";
                    break;
                default:
                    break;
            }

            while (menu_ciklus)
            {
                Console.WriteLine("Remzső Milán, BCNS7O Féléves Feladat: ");
                Console.WriteLine("///////////////////////////////////////");
                Console.WriteLine("Mit szeretnél csinálni a " + tablazat + " táblával?");
                Console.WriteLine("1. Lekérdezni");
                Console.WriteLine("2. Felülírni");
                Console.WriteLine("3. Törölni");
                Console.WriteLine("4. Hozzáadni");
                Console.WriteLine("5. Vissza");
                Console.WriteLine("///////////////////////////////////////");
                Console.Write("Válasz számot: ");
                string input_ketto = Console.ReadLine();

                switch (input_ketto)
                {
                    case "1":
                        if (tablazat == "Beosztottak")
                        {
                            beosztott.Lekerdezes();
                        }
                        else if (tablazat == "Ételek")
                        {
                            etel.Lekerdezes();
                        }
                        else if (tablazat == "Menük")
                        {
                            menu.Lekerdezes();
                        }
                        else if (tablazat == "Sörök")
                        {
                            sor.Lekerdezes();
                        }

                        break;

                    case "2":
                        if (tablazat == "Beosztottak")
                        {
                            beosztott.Frissit();
                        }
                        else if (tablazat == "Ételek")
                        {
                            etel.Frissit();
                        }
                        else if (tablazat == "Menük")
                        {
                            menu.Frissit();
                        }
                        else if (tablazat == "Sörök")
                        {
                            sor.Frissit();
                        }

                        break;

                    case "3":
                        if (tablazat == "Beosztottak")
                        {
                            beosztott.Torol();
                        }
                        else if (tablazat == "Ételek")
                        {
                            etel.Torol();
                        }
                        else if (tablazat == "Menük")
                        {
                            menu.Torol();
                        }
                        else if (tablazat == "Sörök")
                        {
                            sor.Torol();
                        }

                        break;

                    case "4":
                        if (tablazat == "Beosztottak")
                        {
                            beosztott.Hozzaad();
                        }
                        else if (tablazat == "Ételek")
                        {
                            etel.Hozzaad();
                        }
                        else if (tablazat == "Menük")
                        {
                            menu.Hozzaad();
                        }
                        else if (tablazat == "Sörök")
                        {
                            sor.Hozzaad();
                        }

                        break;
                    case "5":
                        menu_ciklus = false;
                        break;
                    default:
                        Console.WriteLine("Iyen menü nem létezik!");
                        System.Threading.Thread.Sleep(800);
                        break;
                }
            }
        }

        private static void Menu_Lekerdezesek()
        {
            Con_Non_Crud con_NonCrud = new Con_Non_Crud();
            Console.Clear();
            string input = string.Empty;
            bool menu_ciklus = true;
            while (menu_ciklus)
            {
                Console.WriteLine("Remzső Milán, BCNS7O Féléves Feladat: ");
                Console.WriteLine("///////////////////////////////////////");
                Console.WriteLine("Lekérdezések: ");
                Console.WriteLine("1. Minden beosztott akinek van söre egy menüben.");
                Console.WriteLine("2. Különböző sörök száma, tipusonként");
                Console.WriteLine("3. Minden alkalmazott akinek a söre 800 ftnál olcsóbb");
                Console.WriteLine("4. Vissza");
                Console.WriteLine("///////////////////////////////////////");
                Console.Write("Válasz számot: ");
                input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        con_NonCrud.Menusör();
                        break;
                    case "2":
                        con_NonCrud.Sörtipusokszama();
                        break;
                    case "3":
                        con_NonCrud.MindenbeosztottAkiSore600FelettVan();
                        break;
                    case "4":
                        menu_ciklus = false;
                        break;
                    default:
                        Console.WriteLine("Iyen menü nem létezik!");
                        System.Threading.Thread.Sleep(800);
                        break;
                }
            }
        }
    }
}
