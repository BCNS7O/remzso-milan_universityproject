﻿// <copyright file="Con_Non_Crud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_bcns7o
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Logic;

    /// <summary>
    /// Deals with the console layer of the Non Crud queries table.
    /// Like Writing to the console, and menus.
    /// </summary>
    internal class Con_Non_Crud
    {
        private readonly Log_Non_Crud logicNonCrud;

        /// <summary>
        /// Initializes a new instance of the <see cref="Con_Non_Crud"/> class.
        /// </summary>
        public Con_Non_Crud()
        {
            this.logicNonCrud = new Log_Non_Crud();
        }

        /// <summary>
        /// Controls the menu of the Menusor query function.
        /// </summary>
        public void Menusör()
        {
            var megoldas = this.logicNonCrud.Menusör();
            foreach (var item in megoldas.ToList())
            {
                Console.WriteLine(item.Név);
            }
        }

        /// <summary>
        /// Controls the menu of the Menusor Sortipusokszama function.
        /// </summary>
        public void Sörtipusokszama()
        {
            var megoldas = this.logicNonCrud.Sörtipusokszama();
            foreach (var item in megoldas.ToList())
            {
                Console.Write(item.Név + " " + item.Átlag);
                Console.WriteLine(" ");
            }
        }

        /// <summary>
        /// Controls the menu of the MindenbeosztottAkiSore600FelettVan query function.
        /// </summary>
        public void MindenbeosztottAkiSore600FelettVan()
        {
            var megoldas = this.logicNonCrud.MindenbeosztottAkiSore800FelettVan();
            foreach (var item in megoldas.ToList())
            {
                Console.WriteLine(item.Név);
            }
        }
    }
}
