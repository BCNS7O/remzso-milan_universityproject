﻿// <copyright file="Con_Menuk.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Oenik_prog3_2019_2_bcns7o
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyBeerHouse.Logic;

    /// <summary>
    /// Deals with the console layer of the Beosztottak table.
    /// Like Writing to the console, and menus.
    /// </summary>
    internal class Con_Menuk
    {
        private readonly Log_Menuk logi;

        /// <summary>
        /// Initializes a new instance of the <see cref="Con_Menuk"/> class.
        /// </summary>
        public Con_Menuk()
        {
            this.logi = new Log_Menuk();
        }

        /// <summary>
        /// Controls the menu of the update function.
        /// </summary>
        public void Frissit()
        {
            Console.WriteLine("A menük tábla frissitése, írd be a menü nevét akit frissiteni akarsz: ");
            string id = Console.ReadLine();
            Console.WriteLine("Add az új Árat: ");
            string input = Console.ReadLine();
            this.logi.Frissit(input, id, string.Empty);
            Console.WriteLine("Tábla Frissitve");
        }

        /// <summary>
        /// Controls the menu of the create function.
        /// </summary>
        public void Hozzaad()
        {
            Console.WriteLine("Add meg az új rekord adatait ebben a sorrendben: Név, Ár, Étel, Ital");
            string ujnev = Console.ReadLine();
            string ujnem = Console.ReadLine();
            string ujfizetes = Console.ReadLine();
            string ujkor = Console.ReadLine();
            string ujcsatlakozas = Console.ReadLine();
            string ujbeoszott_id = Console.ReadLine();
            this.logi.Hozzaad(ujnev, ujnem, ujfizetes, ujkor, ujcsatlakozas, ujbeoszott_id);
        }

        /// <summary>
        /// Controls the menu of the read function.
        /// </summary>
        public void Lekerdezes()
        {
            Console.WriteLine("1. Az egész táblát");
            Console.WriteLine("2. Név szerint.");
            Console.WriteLine("3. Vissza");
            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    Console.WriteLine("Menük: ");
                    Console.WriteLine("Név, Ár, Étel, Ital");
                    string kiir = this.logi.Lekerdezes();
                    Console.WriteLine(kiir);

                    break;
                case "2":
                    Console.WriteLine("Írd be a keresett record nevét:");
                    string ezAlapjanKerdez = Console.ReadLine();
                    Console.WriteLine(ezAlapjanKerdez + ": ");
                    Console.WriteLine("Név, Ár, Étel, Ital");
                    string megoldas = this.logi.Lekerdezes_ById(ezAlapjanKerdez);
                    Console.WriteLine(megoldas);

                    break;
                case "3":
                    break;
                default:
                    Console.WriteLine("Ilyen menü nincs.");
                    break;
            }
        }

        /// <summary>
        /// Controls the menu of the delete function.
        /// </summary>
        public void Torol()
        {
            Console.WriteLine("Táblából törlés, add meg a nevét a sornak, amit törölni akarsz:  ");
            string id = Console.ReadLine();
            this.logi.Torol(id);
            Console.WriteLine("Tábla Frissitve");
        }
    }
}
