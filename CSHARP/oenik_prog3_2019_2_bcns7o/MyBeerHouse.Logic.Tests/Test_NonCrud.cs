﻿// <copyright file="Test_NonCrud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyBeerHouse.Data;
    using MyBeerHouse.Logic;
    using MyBeerHouse.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests the Not Crud queries.
    /// </summary>
    [TestFixture]
    internal class Test_NonCrud
    {
        private static readonly object[] TestCaseListaBeosztottak = new object[]
        {
            new List<Beosztottak>
            {
                new Beosztottak
                {
                    Beosztott_id = "1",
                    Név = "Vona Csaba",
                    Nem = "Férfi",
                    Fizetés = 15000,
                    Kor = 16,
                    Csatlakozás = new DateTime(2017, 01, 23),
                },
                new Beosztottak
                {
                    Beosztott_id = "5",
                    Név = "Pán Péter",
                    Nem = "Nő",
                    Fizetés = 20000,
                    Kor = 24,
                    Csatlakozás = new DateTime(2010, 02, 03),
                },
            },
        };

        private static readonly object[] TestCaseListaEtelek = new object[]
       {
            new List<Ételek>
            {
                new Ételek
                {
                    Ár = 100,
                    Név = "Miccs",
                    Vegetáriánus = "igaz",
                    Glutén = "igaz",
                    DiákKedvezmény = "hamis",
                    Csipős = "igaz",
                },
                new Ételek
                {
                    Ár = 1000,
                    Név = "Leves",
                    Vegetáriánus = "hamis",
                    Glutén = "igaz",
                    DiákKedvezmény = "hamis",
                    Csipős = "hamis",
                },
            },
       };

        private static readonly object[] TestCaseListaMenuk = new object[]
       {
            new List<Menük>
            {
                new Menük
                {
                    Ár = 100,
                    Név = "Miccs",
                    Étel = "Leves",
                    Ital = "Ratass",
                },
                new Menük
                {
                    Ár = 100,
                    Név = "Fincsi",
                    Étel = "Leves",
                    Ital = "Ratass",
                },
            },
       };

        private static readonly object[] TestCaseListaSorok = new object[]
        {
            new List<Sörök>
            {
                new Sörök
                {
                    Ár = 100,
                    Név = "Ratass",
                    Típus = "APA",
                    Alkohol = 11,
                    Beosztott_id = "1",
                    Glutén = "hamis",
                },
                new Sörök
                {
                    Ár = 200,
                    Név = "Fincsi",
                    Típus = "Lager",
                    Alkohol = 11,
                    Beosztott_id = "5",
                    Glutén = "igaz",
                },
            },
        };

        /// <summary>
        /// Checks whether Sörtipusokszama is working.
        /// </summary>
        /// <param name="beosztottakInput">Data that fills the mocked repository.</param>
        /// <param name="sorokInput">Data that fills the mocked repo.</param>
        [Test]
        public void Sörtipusokszama_IsWorking(
            [ValueSource("TestCaseListaBeosztottak")] List<Beosztottak> beosztottakInput,
            [ValueSource("TestCaseListaSorok")] List<Sörök> sorokInput)
        {
            Mock<IRepNonCrud> mrepo = new Mock<IRepNonCrud>();
            mrepo.Setup(x => x.LekerBeosztottak()).Returns(beosztottakInput.AsQueryable());
            mrepo.Setup(x => x.LekerSörök()).Returns(sorokInput.AsQueryable());
            var tesztLogic = new Log_Non_Crud
            {
                RepoNonCrud = mrepo.Object,
            };

            var megoldas = tesztLogic.Sörtipusokszama();
            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.LekerBeosztottak(), Times.AtLeastOnce);
            mrepo.Verify(x => x.LekerSörök(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks whether Menusör is working.
        /// </summary>
        /// <param name="beosztottakInput">Data that fills the mocked repository.</param>
        /// <param name="sorokInput">Data that fills the mocked repo.</param>
        /// <param name="menukInput">Fills the mocked repository with some data.</param>
        [Test]
        public void Menusör_IsWorking(
             [ValueSource("TestCaseListaBeosztottak")] List<Beosztottak> beosztottakInput,
             [ValueSource("TestCaseListaSorok")] List<Sörök> sorokInput,
             [ValueSource("TestCaseListaMenuk")] List<Menük> menukInput)
        {
            Mock<IRepNonCrud> mrepo = new Mock<IRepNonCrud>();
            mrepo.Setup(x => x.LekerBeosztottak()).Returns(beosztottakInput.AsQueryable());
            mrepo.Setup(x => x.LekerSörök()).Returns(sorokInput.AsQueryable());
            mrepo.Setup(x => x.LekerMenük()).Returns(menukInput.AsQueryable());
            var tesztLogic = new Log_Non_Crud
            {
                RepoNonCrud = mrepo.Object,
            };

            var megoldas = tesztLogic.Menusör();
            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.LekerBeosztottak(), Times.AtLeastOnce);
            mrepo.Verify(x => x.LekerSörök(), Times.AtLeastOnce);
            mrepo.Verify(x => x.LekerMenük(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks whether Menusör is working.
        /// </summary>
        /// <param name="beosztottakInput">Data that fills the mocked repository.</param>
        /// <param name="sorokInput">Data that fills the mocked repo.</param>
        [Test]
        public void MindenbeosztottAkiSore800FelettVan_IsWorking(
            [ValueSource("TestCaseListaBeosztottak")] List<Beosztottak> beosztottakInput,
            [ValueSource("TestCaseListaSorok")] List<Sörök> sorokInput)
        {
            Mock<IRepNonCrud> mrepo = new Mock<IRepNonCrud>();
            mrepo.Setup(x => x.LekerBeosztottak()).Returns(beosztottakInput.AsQueryable());
            mrepo.Setup(x => x.LekerSörök()).Returns(sorokInput.AsQueryable());
            var tesztLogic = new Log_Non_Crud
            {
                RepoNonCrud = mrepo.Object,
            };

            var megoldas = tesztLogic.MindenbeosztottAkiSore800FelettVan();
            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.LekerBeosztottak(), Times.AtLeastOnce);
            mrepo.Verify(x => x.LekerSörök(), Times.AtLeastOnce);
        }
    }
}
