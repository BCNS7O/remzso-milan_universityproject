﻿// <copyright file="Test_Etelek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests methods that deal with the Etelek table.
    /// </summary>
    [TestFixture]
    public class Test_Etelek
    {
        private static readonly object[] TestCaseLista = new object[]
       {
            new List<Ételek>
            {
                new Ételek
                {
                    Ár = 100,
                    Név = "Miccs",
                    Vegetáriánus = "igaz",
                    Glutén = "igaz",
                    DiákKedvezmény = "hamis",
                    Csipős = "igaz",
                },
                new Ételek
                {
                    Ár = 1000,
                    Név = "Leves",
                    Vegetáriánus = "hamis",
                    Glutén = "igaz",
                    DiákKedvezmény = "hamis",
                    Csipős = "hamis",
                },
            },
       };

        /// <summary>
        /// Checks if Lekerdezes_By_Id is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_ById_IsWorking(List<Ételek> input)
        {
            Mock<IRepository<Ételek>> mrepo = new Mock<IRepository<Ételek>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Etelek
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes_ById("Miccs");

            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if Lekerdezes is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_IsWorking(List<Ételek> input)
        {
            Mock<IRepository<Ételek>> mrepo = new Mock<IRepository<Ételek>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Etelek
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes();
            Assert.That(mrepo.Object.Lekerdezes(), Is.Not.Null);
            mrepo.Verify(m => m.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Tests that updating a table is working or not.
        /// </summary>
        [Test]
        public void Frissites_IsWorking()
        {
            Mock<IRepository<Ételek>> mrepo = new Mock<IRepository<Ételek>>();
            mrepo.Setup(x => x.Frissit("100", "Mákos Tészta", "1")).Verifiable();
            var tesztLogic = new Log_Etelek
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Frissit("100", "Mákos Tészta", "1"), Is.Not.False);
            mrepo.Verify(x => x.Frissit("100", "Mákos Tészta", "1"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether Deleting a certain record from the table is working.
        /// </summary>
        [Test]
        public void Torles_IsWorking()
        {
            Mock<IRepository<Ételek>> mrepo = new Mock<IRepository<Ételek>>();
            mrepo.Setup(x => x.Torol("Miccs")).Verifiable();
            var tesztLogic = new Log_Etelek
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Torol("Miccs"), Is.Not.False);
            mrepo.Verify(x => x.Torol("Miccs"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether createing a new record for the table is working.
        /// </summary>
        [Test]
        public void Hozzaad_IsWorking()
        {
            Mock<IRepository<Ételek>> mrepo = new Mock<IRepository<Ételek>>();
            mrepo.Setup(x => x.Hozzaad("Makacs Misi", "350", "Igen", "Nem", "Nem", "Igen")).Verifiable();
            var tesztLogic = new Log_Etelek
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Hozzaad("Makacs Misi", "350", "Igen", "Nem", "Nem", "Igen"), Is.Not.False);
            mrepo.Verify(x => x.Hozzaad("Makacs Misi", "350", "Igen", "Nem", "Nem", "Igen"), Times.AtLeastOnce);
        }
    }
}
