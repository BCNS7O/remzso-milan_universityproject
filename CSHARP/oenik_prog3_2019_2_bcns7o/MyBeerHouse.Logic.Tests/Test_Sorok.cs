﻿// <copyright file="Test_Sorok.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests methods that deal with the Sorok table.
    /// </summary>
    [TestFixture]
    public class Test_Sorok
    {
        private static readonly object[] TestCaseLista = new object[]
        {
            new List<Sörök>
            {
                new Sörök
                {
                    Ár = 100,
                    Név = "Ratass",
                    Típus = "APA",
                    Alkohol = 11,
                    Beosztott_id = "1",
                    Glutén = "hamis",
                },
                new Sörök
                {
                    Ár = 200,
                    Név = "Fincsi",
                    Típus = "Lager",
                    Alkohol = 11,
                    Beosztott_id = "5",
                    Glutén = "igaz",
                },
            },
        };

        /// <summary>
        /// Checks if Lekerdezes_By_Id is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_ById_IsWorking(List<Sörök> input)
        {
            Mock<IRepository<Sörök>> mrepo = new Mock<IRepository<Sörök>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Sorok
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes_ById("Ratass");

            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if Lekerdezes is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_IsWorking(List<Sörök> input)
        {
            Mock<IRepository<Sörök>> mrepo = new Mock<IRepository<Sörök>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Sorok
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes();
            Assert.That(mrepo.Object.Lekerdezes(), Is.Not.Null);
            mrepo.Verify(m => m.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Tests that updating a table is working or not.
        /// </summary>
        [Test]
        public void Frissites_IsWorking()
        {
            Mock<IRepository<Sörök>> mrepo = new Mock<IRepository<Sörök>>();
            mrepo.Setup(x => x.Frissit("100", "Ratass", "1")).Verifiable();
            var tesztLogic = new Log_Sorok
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Frissit("100", "Ratass", "1"), Is.Not.False);
            mrepo.Verify(x => x.Frissit("100", "Ratass", "1"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether Deleting a certain record from the table is working.
        /// </summary>
        [Test]
        public void Torles_IsWorking()
        {
            Mock<IRepository<Sörök>> mrepo = new Mock<IRepository<Sörök>>();
            mrepo.Setup(x => x.Torol("Vona Csaba")).Verifiable();
            var tesztLogic = new Log_Sorok
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Torol("Fincsi"), Is.Not.False);
            mrepo.Verify(x => x.Torol("Fincsi"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether createing a new record for the table is working.
        /// </summary>
        [Test]
        public void Hozzaad_IsWorking()
        {
            Mock<IRepository<Sörök>> mrepo = new Mock<IRepository<Sörök>>();
            mrepo.Setup(x => x.Hozzaad("Lepény", "900", "Barna", "11", "4", "Igen")).Verifiable();
            var tesztLogic = new Log_Sorok
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Hozzaad("Lepény", "900", "Barna", "11", "4", "Igen"), Is.Not.False);
            mrepo.Verify(x => x.Hozzaad("Lepény", "900", "Barna", "11", "4", "Igen"), Times.AtLeastOnce);
        }
    }
}
