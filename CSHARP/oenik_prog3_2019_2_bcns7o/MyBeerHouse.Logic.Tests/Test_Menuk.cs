﻿// <copyright file="Test_Menuk.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests methods that deal with the Menuk table.
    /// </summary>
    [TestFixture]
    public class Test_Menuk
    {
        private static readonly object[] TestCaseLista = new object[]
       {
            new List<Menük>
            {
                new Menük
                {
                    Ár = 100,
                    Név = "Miccs",
                    Étel = "Leves",
                    Ital = "Ratass",
                },
                new Menük
                {
                    Ár = 100,
                    Név = "Fincsi",
                    Étel = "Leves",
                    Ital = "Ratass",
                },
            },
       };

        /// <summary>
        /// Checks if Lekerdezes_By_Id is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_ById_IsWorking(List<Menük> input)
        {
            Mock<IRepository<Menük>> mrepo = new Mock<IRepository<Menük>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Menuk
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes_ById("Fincsi");

            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if Lekerdezes is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_IsWorking(List<Menük> input)
        {
            Mock<IRepository<Menük>> mrepo = new Mock<IRepository<Menük>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Menuk
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes();
            Assert.That(mrepo.Object.Lekerdezes(), Is.Not.Null);
            mrepo.Verify(m => m.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Tests that updating a table is working or not.
        /// </summary>
        [Test]
        public void Frissites_IsWorking()
        {
            Mock<IRepository<Menük>> mrepo = new Mock<IRepository<Menük>>();
            mrepo.Setup(x => x.Frissit("100", "Chillzone", "1")).Verifiable();
            var tesztLogic = new Log_Menuk
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Frissit("100", "Chillzone", "1"), Is.Not.False);
            mrepo.Verify(x => x.Frissit("100", "Chillzone", "1"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether Deleting a certain record from the table is working.
        /// </summary>
        [Test]
        public void Torles_IsWorking()
        {
            Mock<IRepository<Menük>> mrepo = new Mock<IRepository<Menük>>();
            mrepo.Setup(x => x.Torol("Chillzone")).Verifiable();
            var tesztLogic = new Log_Menuk
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Torol("Chillzone"), Is.Not.False);
            mrepo.Verify(x => x.Torol("Chillzone"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether createing a new record for the table is working.
        /// </summary>
        [Test]
        public void Hozzaad_IsWorking()
        {
            Mock<IRepository<Menük>> mrepo = new Mock<IRepository<Menük>>();
            mrepo.Setup(x => x.Hozzaad("Makacs Misi", "350", "Miccs", "Mélység", null, null)).Verifiable();
            var tesztLogic = new Log_Menuk
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Hozzaad("Makacs Misi", "350", "Miccs", "Mélység", null, null), Is.Not.False);
            mrepo.Verify(x => x.Hozzaad("Makacs Misi", "350", "Miccs", "Mélység", null, null), Times.AtLeastOnce);
        }
    }
}
