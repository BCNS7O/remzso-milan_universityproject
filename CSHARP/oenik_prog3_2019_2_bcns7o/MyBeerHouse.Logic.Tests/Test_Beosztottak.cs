﻿// <copyright file="Test_Beosztottak.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyBeerHouse.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyBeerHouse.Data;
    using MyBeerHouse.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests methods that deal with the Beosztottak table.
    /// </summary>
    [TestFixture]
    public class Test_Beosztottak
    {
        private static readonly object[] TestCaseLista = new object[]
        {
            new List<Beosztottak>
            {
                new Beosztottak
                {
                    Beosztott_id = "1",
                    Név = "Vona Csaba",
                    Nem = "Férfi",
                    Fizetés = 15000,
                    Kor = 16,
                    Csatlakozás = new DateTime(2017, 01, 23),
                },
                new Beosztottak
                {
                    Beosztott_id = "5",
                    Név = "Pán Péter",
                    Nem = "Nő",
                    Fizetés = 20000,
                    Kor = 24,
                    Csatlakozás = new DateTime(2010, 02, 03),
                },
            },
        };

        /// <summary>
        /// Checks if Lekerdezes_By_Id is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_ById_IsWorking(List<Beosztottak> input)
        {
            Mock<IRepository<Beosztottak>> mrepo = new Mock<IRepository<Beosztottak>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Beosztottak
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes_ById("Vona Csaba");

            Assert.That(megoldas, Is.Not.Null);
            mrepo.Verify(x => x.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Checks if Lekerdezes is working or not.
        /// </summary>
        /// <param name="input">A list that will be used for testing.</param>
        [TestCaseSource("TestCaseLista")]
        public void Lekerdezes_IsWorking(List<Beosztottak> input)
        {
            Mock<IRepository<Beosztottak>> mrepo = new Mock<IRepository<Beosztottak>>();
            mrepo.Setup(x => x.Lekerdezes()).Returns(input.AsQueryable());
            var tesztLogic = new Log_Beosztottak
            {
                Repo = mrepo.Object,
            };

            var megoldas = tesztLogic.Lekerdezes();
            Assert.That(mrepo.Object.Lekerdezes(), Is.Not.Null);
            mrepo.Verify(m => m.Lekerdezes(), Times.AtLeastOnce);
        }

        /// <summary>
        /// Tests that updating a table is working or not.
        /// </summary>
        [Test]
        public void Frissites_IsWorking()
        {
            Mock<IRepository<Beosztottak>> mrepo = new Mock<IRepository<Beosztottak>>();
            mrepo.Setup(x => x.Frissit("42", "Vona Csaba", "Fizetés")).Verifiable();
            var tesztLogic = new Log_Beosztottak
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Frissit("42", "Vona Csaba", "Fizetés"), Is.Not.False);
            mrepo.Verify(x => x.Frissit("42", "Vona Csaba", "Fizetés"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether Deleting a certain record from the table is working.
        /// </summary>
        [Test]
        public void Torles_IsWorking()
        {
            Mock<IRepository<Beosztottak>> mrepo = new Mock<IRepository<Beosztottak>>();
            mrepo.Setup(x => x.Torol("Vona Csaba")).Verifiable();
            var tesztLogic = new Log_Beosztottak
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Torol("Vona Csaba"), Is.Not.False);
            mrepo.Verify(x => x.Torol("Vona Csaba"), Times.AtLeastOnce);
        }

        /// <summary>
        /// Testing whether createing a new record for the table is working.
        /// </summary>
        [Test]
        public void Hozzaad_IsWorking()
        {
            Mock<IRepository<Beosztottak>> mrepo = new Mock<IRepository<Beosztottak>>();
            mrepo.Setup(x => x.Hozzaad("Finom Süti", "Férfi", "10000", "53", "2019.08.23", "74")).Verifiable();
            var tesztLogic = new Log_Beosztottak
            {
                Repo = mrepo.Object,
            };
            Assert.That(tesztLogic.Hozzaad("Finom Süti", "Férfi", "10000", "53", "2019.08.23", "74"), Is.Not.False);
            mrepo.Verify(x => x.Hozzaad("Finom Süti", "Férfi", "10000", "53", "2019.08.23", "74"), Times.AtLeastOnce);
        }
    }
 }